'use strict';

var imports     = require('soop').imports();

var metrocore         = require('metrocore'),
    RpcClient       = metrocore.RpcClient,
    MetrocoreBlock    = metrocore.Block,
	util            = require('util'),
    config          = require('../config/config');

var  metrocoreRpc  = imports.metrocoreRpc || new RpcClient(config.metrocoind);

function Rpc() {
}

Rpc._parseTxResult = function(info) {
  var b  = new Buffer(info.hex,'hex');

  // remove fields we dont need, to speed and adapt the information
  delete info.hex;

  // Inputs => add index + coinBase flag
  var n =0;
  info.vin.forEach(function(i) {
    i.n = n++;
    if (i.coinbase) info.isCoinBase = true;
    if (i.scriptSig) delete i.scriptSig.hex;
  });

  // Outputs => add total
  var valueOutSat = 0;
  info.vout.forEach( function(o) {
    o.value = o.value.toFixed(8);
    valueOutSat += o.value * metrocore.util.COIN;
    delete o.scriptPubKey.hex;
  });
  info.valueOut = valueOutSat.toFixed(0) / metrocore.util.COIN;
  info.size     = b.length;

  return info;
};


Rpc.errMsg = function(err) {
  var e = err;
  e.message += util.format(' [Host: %s:%d User:%s Using password:%s]',
                            metrocoreRpc.host,
                            metrocoreRpc.port,
                            metrocoreRpc.user,
                            metrocoreRpc.pass?'yes':'no'
                          );
  return e;
};

Rpc.getTxInfo = function(txid, doNotParse, cb) {
  var self = this;

  if (typeof doNotParse === 'function') {
    cb = doNotParse;
    doNotParse = false;
  }

  metrocoreRpc.getRawTransaction(txid, 1, function(err, txInfo) {
    // Not found?
    if (err && err.code === -5) return cb();
    if (err) return cb(self.errMsg(err));

    var info = doNotParse ? txInfo.result : self._parseTxResult(txInfo.result);
    return cb(null,info);
  });
};


Rpc.blockIndex = function(height, cb) {
  var self = this;

  metrocoreRpc.getBlockHash(height, function(err, bh){
    if (err) return cb(self.errMsg(err));
    cb(null, { blockHash: bh.result });
  });
};

Rpc.getBlock = function(hash, cb) {
  var self = this;

  metrocoreRpc.getBlock(hash, function(err,info) {
    // Not found?
    if (err && err.code === -5) return cb();
    if (err) return cb(self.errMsg(err));


    if (info.result.height)
      info.result.reward =  MetrocoreBlock.getBlockValue(info.result.height) / metrocore.util.COIN ;

    return cb(err,info.result);
  });
};

Rpc.sendRawTransaction = function(rawtx, cb) {
  var self = this;
  metrocoreRpc.sendRawTransaction(rawtx, function(err, txid) {
    if (err && err.code === -5) return cb(err); // transaction already in block chain
    if (err) return cb(self.errMsg(err));

    return cb(err, txid.result);
  });
};

module.exports = require('soop')(Rpc);


